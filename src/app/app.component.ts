import { partitionArray } from '@angular/compiler/src/util';
import { Component } from '@angular/core';
import {  Pomodoro, PomodoroType  } from './model/pomodoro.model';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  currentPomodoro?: Pomodoro

  allPomodoro: Pomodoro[] = []

  displayTimer = new Date(0)
  countTasks = 0
  para = 0    
  

  stopPomodoro(){
    this.para = 1   
  }

  startPomodoro() {
      
      this.currentPomodoro = new Pomodoro(this.getPomodoroType())
      this.displayTimer = new Date(0)

      setTimeout(() => this.countTime(), 1000)
  }



  countTime() {
    this.displayTimer = new Date(this.displayTimer.getTime() + 1000)
    
    if (this.displayTimer.getTime() > this.currentPomodoro!.maxTime){
      this.currentPomodoro!.finish = new Date()
      this.allPomodoro.push(this.currentPomodoro!)
      this.startPomodoro()      
    } else if (this.para == 0){
      setTimeout(() => this.countTime(), 1000) 
    }else{
      return;
    }

  }

  private getPomodoroType() : PomodoroType{
    if (!this.currentPomodoro || this.currentPomodoro.type != PomodoroType.TASK){
      this.countTasks ++
      return PomodoroType.TASK
    } else if (this.countTasks == 2){
      this.countTasks = 0
      return PomodoroType.LONG_BREAK
    }

    return PomodoroType.BREAK
  }
}


